function load_colour_scheme(theme) {
  load_json(function(response) {
    theme_colours = JSON.parse(response);
  }, 'jsh/themes/' + theme + '.json');

  document.body.style.background = theme_colours["background"]
  document.body.style.color = theme_colours["text"]
  document.getElementById("terminal_input").style.background = theme_colours["background"]
  document.styleSheets[0].insertRule(".prompt_id { color: " + theme_colours["prompt_id"] + "; }", 1);
  document.styleSheets[0].insertRule(".workingDirectory { color: " + theme_colours["workingDirectory"] + "; }", 1);
  document.styleSheets[0].insertRule(".ls_file { color: " + theme_colours["ls_file"] + "; }", 1);
  document.styleSheets[0].insertRule(".ls_directory { color: " + theme_colours["ls_directory"] + "; }", 1);
}