var available_commands = ["help", "clear", "whoami", "hostname", "pwd", "ls",
  "dir", "cat", "type", "cd", "b64e", "b64d"
]

function parse_input(input) {
  args = input.trim().split(" ")
  switch (args[0]) {
    case "":
      break;
    case "help":
      help()
      break;
    case "cls":
    case "clear":
      clear()
      break;
    case "whoami":
      whoami()
      break;
    case "hostname":
      hostname()
      break;
    case "pwd":
      pwd()
      break;
    case "ls":
    case "dir":
      ls(args.slice(1, args.length))
      break;
    case "cat":
    case "type":
      if (args.length > 1) {
        cat(args.slice(1, args.length))
      } else {
        add_to_terminal("Usage: [cat | type] [path/to/file]<br />")
      }
      break;
    case "cd":
      if (args.length > 1) {
        cd(args.slice(1, args.length))
      } else {
        cd('~')
      }
      break;
    case "b64e":
      if (args.length > 1) {
        add_to_terminal(btoa(args.slice(1, args.length).join(' ')) + "<br />")
      } else {
        add_to_terminal("Usage: b64e [string]<br />")
      }
      break;
    case "b64d":
      if (args.length > 1) {
        add_to_terminal(atob(args[1]) + "<br />")
      } else {
        add_to_terminal("Usage: b64d [string]<br />")
      }
      break;
    default:
      unknown_command(args[0])
      break;
  }
}

function help() {
  help_string = `jsh v1.0<br />
Available commands:<br />
<table class="help">
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td>help</td><td>prints this help message</td></tr>
<tr><td></td><td>clear</td><td>clears the terminal</td></tr>
<tr><td></td><td>whoami</td><td>display effective user id</td></tr>
<tr><td></td><td>hostname&nbsp;&nbsp;&nbsp;&nbsp;</td><td>print the name of the host system</td></tr>
<tr><td></td><td>pwd</td><td>print the working directory path</td></tr>
<tr><td></td><td>ls, dir</td><td>list directory contents</td></tr>
<tr><td></td><td>cat, type</td><td>print file contents</td></tr>
<tr><td></td><td>cd</td><td>change directory</td></tr>
<tr><td></td><td>b64e</td><td>base64 encode the given string</td></tr>
<tr><td></td><td>b64d</td><td>base64 decode the given string</td></tr>
</table>`
  add_to_terminal(help_string)
}

function clear() {
  document.getElementById("terminal").innerHTML = ""
}

function whoami() {
  add_to_terminal("user<br />")
}

function hostname() {
  add_to_terminal("scionti.net<br />")
}

function pwd() {
  add_to_terminal(current_directory + "<br />")
}

function unknown_command(command) {
  add_to_terminal("-jsh: " + command + ": command not found<br />")
}

function ls(params) {
  arguents = []
  targets = []
  show_all = false
  show_as_list = false
  // recurse = false
  // if recurse add targets under args
  if (params.length != 0) {
    for (i = 0; i < params.length; i++) {
      if (params[i][0] == '-') {
        args = params[i].slice(1, params[i].length)
        for (j = 0; j < args.length; j++) {
          switch (args[j]) {
            case 'a':
              show_all = true
              break;
            case 'l':
              show_as_list = true
              break;
          }
        }
      } else {
        targets.push(params[i])
      }
    }
  }
  if (targets.length == 0) {
    targets = [current_directory]
  }
  var string_to_add = "";
  for (target in targets) {
    var result = ""
    result = get_target(targets[target])
    if (!result) {
      string_to_add += "ls: " + targets[target] + ": No such file or directory"
    } else if (typeof(result) == "string") {
      string_to_add += targets[target]
    } else {
      if (targets.length > 1) {
        string_to_add += targets[target] + ":<br />"
      }
      Object.keys(result).forEach(function(key, _) {
        if ((key[0] != ".") || show_all) {
          if (!show_as_list) {
            switch (typeof(result[key])) {
              case "string":
                string_to_add += "<span class=\"ls_file\">" + key + "&nbsp;&nbsp;&nbsp;&nbsp;</span>"
                break;
              case "object":
                string_to_add += "<span class=\"ls_directory\">" + key + "&nbsp;&nbsp;&nbsp;&nbsp;</span>"
                break;
            }
          } else {
            switch (typeof(result[key])) {
              case "string":
                string_to_add += "<span class=\"ls_file\">-rw-r--r--  user  " + key + "</span><br />"
                break;
              case "object":
                string_to_add += "<span class=\"ls_directory\">drw-r--r--  user  " + key + "</span><br />"
                break;
            }
          }
        }
      });
    }
    if (!(show_as_list || show_all)) {
      string_to_add += "<br />"
    }
  }
  add_to_terminal(string_to_add)
}

function cat(targets) {
  if (targets) {
    for (target in targets) {
      result = get_target(targets[target])
      if (result) {
        if (typeof(result) == "string") {
          add_to_terminal(result + "<br />")
        } else {
          add_to_terminal("cat: " + targets[target] + ": Is a directory<br />")
        }
      } else {
        add_to_terminal("cat: " + targets[target] + ": No such file or directory<br />")
      }
    }
  }
}

function cd(target) {
  if (target == "~") {
    current_directory = "/home/user"
  } else {
    typeofTarget = typeof(get_target(target[0]))
    if (typeofTarget == "object") { // target exists and is a directory
      current_directory = get_target(target[0], "")
    } else if (typeofTarget == "string") { // target exists but is a file
      add_to_terminal("-jsh: cd: " + target[0] + ": Not a directory<br />")
    } else {
      add_to_terminal("-jsh: cd: " + target[0] + ": No such file or directory<br />")
    }
  }
  if (current_directory == "/home/user") {
    set_prompt("~");
  } else {
    new_prompt = current_directory.split("/")
    set_prompt(new_prompt[new_prompt.length - 1]);
  }
}
