var current_directory = "/home/user";
var filesystem;
var command_history = []
var history_scroll_position = 0
var theme_colours;
var banner_items;

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function focus_terminal_input() {
  document.getElementById('terminal_input').focus();
}
window.onload = function() {
  // Credit: https://codepen.io/atelierbram/pen/hbyAd
  document.documentElement.setAttribute('data-userAgent', navigator.userAgent);
  if (navigator.userAgent.match(/Mobile/i)) {
    document.body.onclick = function() {
    //document.body.addEventListener('click', focus_terminal_input(), true);
      focus_terminal_input()
    }
  }

  load_colour_scheme("materialdark");
  load_banner();
  load_filesystem();
  init();
};

async function init() {
  for (item in banner_items) {
    add_to_banner(banner_items[item])
    await sleep(600);
  }
  await sleep(500);
  set_prompt("~");
  keep_terminal_focused();
  catch_terminal_modifier();
  document.getElementById("terminal_input").focus();
}

function catch_terminal_modifier() {
  var terminal_input = document.getElementById("terminal_input")
  terminal_input.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) { // "enter"
      add_to_terminal(document.getElementById("prompt").innerHTML + " " + terminal_input.value + "<br />")
      parse_input(terminal_input.value)
      command_history.unshift(terminal_input.value)
      history_scroll_position = 0
      terminal_input.value = ""
      document.getElementById("terminal_input").style.width = parseFloat(document.defaultView.getComputedStyle(document.getElementById("container")).width, 10) - document.getElementById("prompt").clientWidth - 20 + "px"
      window.scrollTo(0, document.body.scrollHeight); // keep scroll at the bottom of the page
    } else if (event.keyCode === 38) { // "up"
      if ((command_history.length != 0) && (command_history.length >= (history_scroll_position + 1))) {
        terminal_input.value = command_history[history_scroll_position]
        history_scroll_position += 1
      }
    } else if (event.keyCode === 40) { // "down"
      if ((command_history.length != 0) && (history_scroll_position != 0)) {
        history_scroll_position -= 1
        terminal_input.value = command_history[history_scroll_position]
      }
    } else if (event.keyCode == 9) { // "tab"
      args = terminal_input.value.split(" ")
      if (args.length == 1) { // if input is only the command
        for (command in available_commands) { // check available_commands
          if (available_commands[command].startsWith(args[args.length - 1])) {
            args[args.length - 1] = available_commands[command]
          }
        }
      } else { // else attempt to complete the path
        if (args[args.length - 1][0] == "/" || args[args.length - 1][0] == "~") {
          full_path = args[args.length - 1]
        } else {
          full_path = current_directory + "/" + args[args.length - 1]
        }
        if (full_path.slice(-1) == "/") {
            add_to_terminal(document.getElementById("prompt").innerHTML + " " + terminal_input.value + "<br />")
            ls([full_path])
        } else {
            full_path_split = full_path.split("/")
            full_path_directory_contents = get_target(full_path_split.slice(0, full_path_split.length - 1).join("/"))
            for (key of Object.keys(full_path_directory_contents)) {
              last_arg = args[args.length - 1]
              arg_path_split = []
              if (last_arg.includes("/")) {
                arg_path_split = last_arg.split("/")
                last_arg = arg_path_split[arg_path_split.length - 1]
              }
              if (key.startsWith(last_arg)) {
                if (arg_path_split.length != 0) {
                    arg_path_split[arg_path_split.length - 1] = key
                    args[args.length - 1] = arg_path_split.join("/")
                } else {
                  args[args.length - 1] = key
                  if (typeof(full_path_directory_contents[key]) != "string") {
                      args[args.length - 1] += "/"
                  }
                }
                break
              }
            }
         }
      }
      terminal_input.value = args.join(" ")
    }
  });
}

function keep_terminal_focused() {
  var keysDown = {};
  var terminal_input = document.getElementById("terminal_input")
  document.body.addEventListener("keyup", function(event) {
    keysDown[event.keyCode] = false
  });
  document.body.addEventListener("keydown", function(event) {
    if (event.keyCode === 9) {
      event.preventDefault();
    }
    keysDown[event.keyCode] = true
    if ((keysDown[91] || keysDown[17] || keysDown[224]) && keysDown[86]) {
      // if ctrl+v (windows) or command+v (mac), focus terminal_input
      terminal_input.focus();
    } else if (!(keysDown[16] || // shift
        keysDown[17] || // ctrl
        keysDown[18] || // alt
        keysDown[20] || // caps lock
        keysDown[27] || // escape
        keysDown[91] || // windows key
        keysDown[224] || // mac command key
        keysDown[92] || // select
        keysDown[93])) {
      // else if non-modifier key pressed down
      terminal_input.focus();
    }
  });
}

function set_prompt(workingDirectory) {
  document.getElementById("prompt").innerHTML = "<span class=\"prompt_id\">user@scionti.net</span>:<span class=\"workingDirectory\">" + workingDirectory + "</span>$";
  document.getElementById("terminal_input").style.width = parseFloat(document.defaultView.getComputedStyle(document.getElementById("container")).width, 10) - document.getElementById("prompt").clientWidth - 32 + "px"
}

function add_to_terminal(input) {
  document.getElementById("terminal").innerHTML += input
}

function add_to_banner(input) {
  document.getElementById("banner").innerHTML += input
}

function load_filesystem() {
  load_json(function(response) {
    filesystem = JSON.parse(response);
  }, 'filesystem.json');
}

function load_banner() {
  load_json(function(response) {
    banner_items = JSON.parse(response)
  }, "banner.json");
}

function load_json(callback, target) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', target, false);
  xobj.onreadystatechange = function() {
    if (xobj.readyState == 4 && xobj.status == "200") {
      callback(xobj.responseText);
    }
  }
  xobj.send(null);
}

function get_target(target, path) {
  var result = ''
  switch (target) {
    case "/":
      result = filesystem
      break;
    case "~":
      if (typeof(path) != "undefined") {
        result = trace_target(filesystem["home"], ["user"], "/home")
      } else {
        result = trace_target(filesystem["home"], ["user"], path)
      }
      break;
    case ".":
      cdSplit = current_directory.split("/")
      result = trace_target(filesystem, cdSplit.slice(1, cdSplit.length), path)
      break;
    default:
      switch (target[0]) {
        case "/":
          targetSplit = target.slice(1, target.length).split("/")
          result = trace_target(filesystem, targetSplit, path)
          break;
        case "~":
          targetSplit = target.split("/")
          result = trace_target(filesystem["home"]["user"], targetSplit.slice(1, targetSplit.length), path)
          break;
        default:
          if (target.slice(0, 2) == "./") {
            target = target.slice(2, target.length)
          }
          pathSplit = (current_directory + "/" + target).split("/")
          result = trace_target(filesystem, pathSplit.slice(1, pathSplit.length), path)
      }
  }
  return result
}

function trace_target(start, target, path) {
  if (target.length == 1 && target[0] == "") { // catches paths ending in '/'
    if (typeof(path) != undefined) {
      return path
    } else {
      return start
    }
  }
  if (target.length > 1 && target[1] == "..") {
    if (target.length > 2) {
      if (typeof(path) != "undefined") {
        return trace_target(start, target.slice(2, target.length), path)
      } else {
        return trace_target(start, target.slice(2, target.length))
      }
    } else {
      if (typeof(path) != "undefined") {
        return path
      } else {
        return start
      }
    }
  }
  target = target.filter(p => p !== ".").filter(p => p !== "")
  if (start.hasOwnProperty(target[0])) {
    if (typeof(start[target[0]]) == "string" || target.length == 1) {
      if (typeof(path) != "undefined") {
        return [path, target[0]].join("/")
      } else {
        return start[target[0]]
      }
    } else {
      if (typeof(path) != "undefined") {
        return trace_target(start[target[0]], target.slice(1, target.length), [path, target[0]].join("/"))
      } else {
        return trace_target(start[target[0]], target.slice(1, target.length))
      }
    }
  }
}
